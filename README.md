# Puzzle Project

A python jigsaw puzzle solver


## Quick Start

```
$ python3 -m venv env
$ source ./env/bin/activate
$ pip install -r requirements.txt
$ jupyter lab Extract.ipynb
```

## Demo 

See [exported notebook here](Extract.md).
