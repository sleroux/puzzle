numpy              == 1.18.1
seaborn            == 0.10.0
opencv-python      == 4.1.2.30
matplotlib         == 3.1.2
jupyter            == 1.0.0
jupyter-client     == 5.3.4
jupyter-console    == 6.1.0
jupyter-core       == 4.6.1
jupyterlab         == 1.2.6
jupyterlab-server  == 1.0.6
imutils